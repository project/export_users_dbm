#!/usr/bin/php5
<?php

$user = trim(fgets(STDIN, 100));
$groups = trim(fgets(STDIN, 100));

if (!$user || !$groups) {
  exit(1);
}

$id = dba_open("/tmp/groups.db", 'r', 'db4');
if (!$id) {
  exit(1);
}

$db_groups = dba_fetch($user, $id);
if (!$db_groups) {
  exit(1);
}

$db_groups = explode(',', $db_groups);

foreach (explode(' ', $groups) as $group) {
  foreach ($db_groups as $candidate_group) {
    if ($group === $candidate_group) {
      exit(0);
    }
  }
}

exit(1);

