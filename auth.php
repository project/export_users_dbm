#!/usr/bin/php5
<?php

$user = trim(fgets(STDIN, 100));
$pass = trim(fgets(STDIN, 100));

if (!$user || !$pass) {
  exit(1);
}

$id = dba_open("/tmp/users.db", 'r', 'db4');
if (!$id) {
  exit(1);
}

$db_password = dba_fetch($user, $id);
if (!$db_password) {
  exit(1);
}

if ($db_password == md5($pass)) {
  exit(0);
}
else {
  exit(1);
}
